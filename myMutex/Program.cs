using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace myMutex
{
    class Program
    {

        static  Mutex myMutex= new Mutex();
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            // Console.WriteLine("Hello World!");
            // Console.ReadKey();


            Thread ping = new Thread(TakeTurn);
            Thread pong = new Thread(TakeTurn);
            ping.Name = "Ping";
            pong.Name = "Pong";
            ping.Start();
            pong.Start();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 
        }

        static void TakeTurn()
        {
            myMutex.WaitOne();

            Console.WriteLine($"{Thread.CurrentThread.Name} started");
            Console.WriteLine($"{Thread.CurrentThread.Name} stopped");
            //Monitor.Exit(myMutex);
            myMutex.ReleaseMutex();



        }
    }
}
